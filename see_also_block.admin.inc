<?php

/*
 * @file
 * Admin pages.
 */
/*
 * List of blocks.
 */
function see_also_block_list_bloks() {
  global $conf;

  $query = db_select('block', 'u')
    ->fields('u', array('bid', 'module', 'delta'))
    ->condition('theme', $conf['theme_default']);
  $result = $query->execute()->fetchAllAssoc('bid');
  $blocks = module_invoke_all('block_info');

$prepared_blocks=array();
  foreach ($result as $key => $record) {
    $result[$key] = (array)$record;

    $prepared_blocks[$result[$key]['module'].':'.$result[$key]['delta']] =$blocks[$result[$key]['delta']]['info'];
  }

  return $prepared_blocks;
}

/*
 * Settings page.
 */
function see_also_block_settings($form, $form_state) {
  $blocks = see_also_block_list_bloks();
  /*$blocks = module_invoke_all('block_info');
  dsm($blocks);
  foreach ($blocks as $delta => $block) {
    if (is_string($block['info'])) {
      $blocks[$delta] = $block['info'];
    }
    else {
      unset($blocks[$delta]);
    }
  }
  $block_placeholder[0]=t('Block placeholder');
  $block+=$block_placeholder;*/
  $form['see_also_block_activate'] = array(
    '#type' => 'checkbox',
    '#title' => t('See also block activation'),
    '#default_value' => variable_get('see_also_block_activate', SEE_ALSO_BLOCK_ACTIVATION),
  );

  $form['see_also_block_min_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Minimum activation height:'),
    '#default_value' => variable_get('see_also_block_min_height', SEE_ALSO_BLOCK_MIN_HEIGHT),
  );
  $form['see_also_block_animation_speed'] = array(
    '#type' => 'textfield',
    '#title' => t('Animation speed:'),
    '#default_value' => variable_get('see_also_block_animation_speed', SEE_ALSO_BLOCK_ANIMATION_SPEED),
  );
  $form['see_also_block_block_container'] = array(
    '#type' => 'select',
    '#title' => t('Animation speed:'),
    '#options' => $blocks,
    '#default_value' => variable_get('see_also_block_block_container', SEE_ALSO_BLOCK_BLOCK_CONTAINER),
  );
  return system_settings_form($form);
}
