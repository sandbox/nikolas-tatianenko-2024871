<div class="see_also_block" style="background:#000;">
	<button class="see_also_block-close"><?php print $close;?></button>
	<div class="see_also_block-header">
		<h2 class='block-title'><?php print $title; ?></h2>
   	</div>
   	<div class="see_also_block-content">
   		<?php print $content; ?>
   </div>
</div>