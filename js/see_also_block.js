/**
 * @file
 * See also javascript.s
 */
(function ($) {
  var defaults = {
    min: 500,
    containerID: '.see_also_block',
    containerHoverID: 'toTopHover',
    animationSpeed: 500,
    easingType: 'linear'
  };
  //settings = $.extend(defaults, options),
  settings = defaults,
    containerIDhash = '.see_also_block',
    containerHoverIDHash = '#' + settings.containerHoverID;
  $(window).scroll(function () {

    var curren_height = $(window).scrollTop();
    if (typeof document.body.style.maxHeight === "undefined") {
      $(settings.containerID).animate({
        'left': 0
      }, 500, settings.easingType);
    }
    if (curren_height > settings.min) {
      $(settings.containerID).animate({
        'left': 0
      }, 500, settings.easingType);
      console.log('fadeIn');
    }
    else {
      $(settings.containerID).animate({
        'left': -1000
      }, 500, settings.easingType);
      console.log('fadeOut');
    }
  });
})(jQuery);

